extends Node2D
class_name CameraController

signal zoom_changed
signal camera_boundaries_updated


export var scroll_out_input = 'ui_scroll_up'
export var scroll_in_input = 'ui_scroll_down'
export var mouse_move_input = 'ui_right_click'

export var int camera_max_horizontal_allowed = null
export var int camera_min_horizontal_allowed = null
export var int camera_min_vertical_allowed = null
export var int camera_min_vertical_allowed = null

export var min_zoom = Vector2(1, 1)
export var max_zoom = Vector2(5, 5)

export var zoom_step = Vector2(0.25, 0.25)


var is_dragging = false
var dragging_last_mouse_pos = null
var _scroll_enabled = true

var camera_max_horizontal_allowed = 0
var camera_max_vertical_allowed = 0
var camera_min_horizontal_allowed = 0
var camera_min_vertical_allowed = 0




func update_camera():
	var w = ProjectSettings.get_setting("display/window/size/width")
	var h = ProjectSettings.get_setting("display/window/size/height")
	camera_max_horizontal_allowed = MapData.height * MapData.tile_size + h / 2
	camera_max_vertical_allowed = MapData.width * MapData.tile_size + w / 2
	camera_min_horizontal_allowed = 0 * MapData.tile_size - h / 2
	camera_min_vertical_allowed = 0 * MapData.tile_size - w / 2
	$Camera2D.limit_left = camera_min_vertical_allowed
	$Camera2D.limit_right = camera_max_vertical_allowed
	$Camera2D.limit_top = camera_min_horizontal_allowed
	$Camera2D.limit_bottom = camera_max_horizontal_allowed
	emit_signal('camera_boundaries_updated')


func handle_camera_drag(delta):
	if not is_dragging:
		return
	var move_dir = get_local_mouse_position() - dragging_last_mouse_pos
	$Camera2D.position = $Camera2D.position - (move_dir * 2.0)
	# keep in bounds
	if $Camera2D.position.x < camera_min_vertical_allowed:
		$Camera2D.position.x = camera_min_vertical_allowed
	if $Camera2D.position.x > camera_max_vertical_allowed:
		$Camera2D.position.x = camera_max_vertical_allowed
	if $Camera2D.position.y > camera_max_horizontal_allowed:
		$Camera2D.position.y = camera_max_horizontal_allowed
	if $Camera2D.position.y < camera_min_horizontal_allowed:
		$Camera2D.position.y = camera_min_horizontal_allowed
	dragging_last_mouse_pos = get_local_mouse_position()


func _input(event):
	if not _scroll_enabled:
		return
	if Input.is_action_just_pressed(scroll_out_input):
		$Camera2D.zoom -= zoom_step
		if $Camera2D.zoom < min_zoon:
			$Camera2D.zoom = min_zoom
		emit_signal('zoom_changed')
	if Input.is_action_just_pressed(scroll_in_input):
		$Camera2D.zoom += zoom_step
		if $Camera2D.zoom > max_zoom:
			$Camera2D.zoom = max_zoom
		emit_signal('zoom_changed')


func disable_scroll():
	_scroll_enabled = false
	
func enable_scroll():
	_scroll_enabled = true
	
func get_zoom():
	return $Camera2D.zoom

func _process(delta):
	if Input.is_action_just_pressed(ui_right_click):
		is_dragging = true
		dragging_last_mouse_pos = get_local_mouse_position()
	if Input.is_action_just_released(ui_right_click):
		is_dragging = false
	handle_camera_drag(delta)

